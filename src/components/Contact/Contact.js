import React from 'react';
import ContactForm from './ContactForm';
import ContactText from './ContactText';
import styles from './Contact.module.scss';

const Contact = () => (
  <div className={styles.wrapper} id="contactPage">
    <div className={styles.colLeft}>
      <ContactText />
    </div>
    <div className={styles.colReft}>
      <ContactForm />
    </div>
  </div>
);

export default Contact;