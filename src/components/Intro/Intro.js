import React, { Component } from 'react';
import styles from './Intro.module.scss';
import { Link } from "react-scroll";

class Intro extends Component {

    render () {
        return (
            <div className={styles.wrapper} id="section-0">
                <div className={styles.content}>
                    <h1>Strony internetowe Rybnik</h1>
                    <div>
                        <p>Rozwiń swoją firmę w Internecie.</p>
                        <p>Tworzymy strony internetowe dla klientów z Rybnika i okolic</p>
                        <p>Profesjonalne usługi zwiększające ilość klientów, które znajdziesz w jednym miejscu.</p>
                        <p>Od analizy, przez projekt po marketing w wyszukiwarkach.</p>
                    </div>
                    <p className={styles.marginTop}>
                        <a href="mailto:info@szymonsobik.pl?Subject=Zapytanie" className={styles.btn}>Napisz do nas</a>
                        <span className={styles.lub}>lub zadzwoń</span>
                        <span id="clickToShow" className={styles.clickToShow}>530 919 728</span>
                    </p>
                    <p className={styles.marginTopMobile}>
                    <a href="tel:+48530919728" target="_blank" rel="noopener noreferrer" className={styles.btn}>Zadzwoń do nas</a>
                        <span className={styles.lub}>lub napisz na</span>
                        <a href="mailto:info@szymonsobik.pl?Subject=Zapytanie" className={styles.mobileLink}>info@szymonsobik.pl</a>
                    </p>
                </div>
                <div className={styles.introTxt}>
                    <p><Link
                            to="bePromotedPage"
                            smooth={true}
                            duration={800}
                            offset={-150}
                        >Przewiń w dół</Link>
                    </p>
                </div>
            </div>
        )
    }
};

export default Intro;