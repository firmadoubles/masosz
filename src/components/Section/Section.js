import React from 'react';
import styles from './Section.module.scss';

const Section = (props) => (
    <div className={styles.wrapper} id={ props.id }>
        <p>{ props.text }</p>
    </div>
);

export default Section;