import React from 'react'
import styles from './ModernTechnologiesText.module.scss'

const ModernTechnologiesText = () => (
    <div className={styles.wrapper}>
        <h2>Więcej niż <strong>strony internetowe Rybnik</strong></h2>
        <p>Niektórzy odwiedzą stronę internetową korzystając z telefonu czy tabletu, kolejni mogą używać laptopa lub standardowego komputera. Jednych przekona merytoryczny tekst, innych ciekawe animacje. Dlatego wszystkie tworzone przez nas strony internetowe są dostosowane do urządzeń mobilnych.</p>
        <p>Korzystamy tylko z najnowocześniejszych technologii dlatego nasze strony internetowe są szybkie, bezpieczne i dostępne.</p>
        <p>Strony internetowe, które kodujemy nie opierają się na gotowych rozwiązaniach takich jak page buildery czy darmowe bądź płatne motywy. Tworzymy strony www od podstaw - dopasowane i zoptymalizowane.</p>
    </div>
);

export default ModernTechnologiesText;