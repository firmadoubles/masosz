import React from 'react';
import styles from './ContactText.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClock, faMobileAlt, faEnvelope } from '@fortawesome/free-solid-svg-icons';

const ContactText = () => (
  <div className={styles.wrapper}>
    <div className={styles.contact__row}>
      <h3>Dane kontaktowe</h3>
      <p>
        <span className={styles.contact__ico}>
          <FontAwesomeIcon icon={faMobileAlt} />
        </span>
        <a href="tel:+48530919728">+48 530 919 728</a>
      </p>
      <p>
        <span className={styles.contact__ico}>
          <FontAwesomeIcon icon={faEnvelope} />
        </span>
        <a href="mailto:info@szymonsobik.pl">info@szymonsobik.pl</a>
      </p>
    </div>
    <div className={styles.contact__row}>
      <h3>Godziny pracy</h3>
      <p>Poniedziałek - Sobota:</p>
      <p>
        <span className={styles.contact__ico}>
          <FontAwesomeIcon icon={faClock} />
        </span>
        8:00 - 20:00
      </p>
    </div>
    <div className={styles.contact__row}>
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2562.1493715230636!2d18.568718815716768!3d50.04603307942158!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47114fc68205642d%3A0x5ac816c12f6e2342!2sStrony%20internetowe%20Rybnik%20-%20Tworzenie%20stron%20www!5e0!3m2!1spl!2spl!4v1617020740213!5m2!1spl!2spl" width="400" height="250" allowfullscreen="" loading="lazy" title="Mapa" className={styles.google_map}></iframe>
    </div>
  </div>
);

export default ContactText;