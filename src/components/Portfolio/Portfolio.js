import React from 'react'
import styles from './Portfolio.module.scss'
import PortfolioBox from './PortfolioBox'
import osiedleczarneckiegoImg from '../../assets/osiedle-czarneckiego.webp'
import agatagossImg from '../../assets/agata-goss.webp'
import malgorzatalibudaImg from '../../assets/malgorzata-libuda.webp'
import kacprzykImg from '../../assets/kacprzyk.webp'
import kasperczykaImg from '../../assets/kasperczyka.webp'
import zzposzImg from '../../assets/zzposz.webp'
import tkoczImg from '../../assets/tkocz.webp'
import krystynkaImg from '../../assets/krystynka.webp'
import slowackiImg from '../../assets/slowacki.webp'
import antenowyImg from '../../assets/antenowy.webp'
import munduryImg from '../../assets/mundury.webp'
import ziolomedImg from '../../assets/ziolomed.webp'

const Portfolio = () => (
    <div className={styles.wrapper} id="portfolioPage">
        <h2 className={styles.title}>Portfolio strony internetowe Rybnik</h2>
        <div className={styles.portfolioList}>
            <PortfolioBox
                link="kacprzyk-wyburzenia.pl"
                title="strony internetowe Rybnik"
                img={kacprzykImg}
            />
            <PortfolioBox
                link="zzposz.pl"
                title="strony internetowe Rybnik"
                img={zzposzImg}
            />
            <PortfolioBox
                link="energia-naturalnie.pl"
                title="strony internetowe Rybnik"
                img={tkoczImg}
            />
            <PortfolioBox
                link="osiedleczarneckiego.pl"
                title="najlepsze strony internetowe Rybnik"
                img={osiedleczarneckiegoImg}
            />
            <PortfolioBox
                link="mundurydominiec.pl"
                title="profesjonalne tworzenie stron Rybnik"
                img={munduryImg}
            />
            <PortfolioBox
                link="osiedlekasperczyka.pl"
                title="najlepsze strony internetowe Rybnik"
                img={kasperczykaImg}
            />
            <PortfolioBox
                link="agatagoss.pl"
                title="najlepsze strony www Rybnik"
                img={agatagossImg}
            />
            <PortfolioBox
                link="malgorzatalibuda.com"
                title="profesjonalne tworzenie stron Rybnik"
                img={malgorzatalibudaImg}
            />
            <PortfolioBox
                link="wozkidzieciecerybnik.pl"
                title="profesjonalne tworzenie stron Rybnik"
                img={krystynkaImg}
            />
            <PortfolioBox
                link="chorslowacki.pl"
                title="profesjonalne tworzenie stron Rybnik"
                img={slowackiImg}
            />
            <PortfolioBox
                link="serwisantenowy.pl"
                title="profesjonalne tworzenie stron Rybnik"
                img={antenowyImg}
            />
            <PortfolioBox
                link="ziolomed.sklep.pl"
                title="profesjonalne tworzenie stron Rybnik"
                img={ziolomedImg}
            />
        </div>
    </div>
);

export default Portfolio;