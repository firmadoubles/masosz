import React from 'react';
import styles from './PortfolioBox.module.scss'

const PortfolioBox = (props) => (
    <div className={styles.wrapper}>
        <figure className={styles.thumb}>
            <img
                src={props.img}
                alt={props.title}
                title={props.title}
            />
        </figure>
        <p className={styles.link}><a href={`https://${props.link}/`} target="_blank" rel="noopener noreferrer">{props.link}</a></p>
    </div>
);

export default PortfolioBox;