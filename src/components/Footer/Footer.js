import React from 'react';
import styles from './Footer.module.scss';

const Footer = () => (
  <footer className={styles.wrapper}>
    <p>Copyright www.szymonsobik.pl - strony internetowe Rybnik &copy; 2015 - 2022</p>
  </footer>
);

export default Footer;