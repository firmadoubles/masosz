import React from 'react';
import styles from './BePromoted.module.scss';
// import bePromotedImg from '../../assets/wypromuj-strone.webp';
import BePromotedText from './BePromotedText';

const BePromoted = () => (
    <div className={styles.wrapper} id="bePromotedPage">
        <BePromotedText />
        <div class="fb-page" data-href="https://www.facebook.com/stronyinternetowerybnik" data-tabs="timeline" data-width="500" data-height="400" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
            <blockquote cite="https://www.facebook.com/stronyinternetowerybnik" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/stronyinternetowerybnik">Strony internetowe Rybnik - tworzenie stron www</a></blockquote>
        </div>
    </div>
);

export default BePromoted;