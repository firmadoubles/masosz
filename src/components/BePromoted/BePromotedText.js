import React from 'react';
import styles from './BePromotedText.module.scss';

const BePromotedText = () => (
    <div className={styles.wrapper}>
        <h2>Strony internetowe Rybnik</h2>
        <p className={styles.bigText}>Wypromuj się w <span>Internecie</span></p>
        <p>Strony internetowe to dopiero początek. Na ich sukces składa się atrakcyjny wygląd, intuicyjna obsługa, szybkość działania, odpowiednia treść oraz rozwiązania technologiczne.</p>
        <p>Co więcej, nawet najlepszy projekt nie przyniesie korzyści bez odpowiedniej promocji w Internecie. Dlatego proponujemy Ci kompleksową obsługę w zakresie nowych mediów.</p>
    </div>
);

export default BePromotedText;